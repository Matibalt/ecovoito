# Ecovoito - Matéo.B - Lucas.T - Elise.G



## Qu'est ce que Ecovoito ?

Ecovoito est un challenge inter-entreprise sur le nombre de kilomètres économisés lors du transport domicile-travail des salariés.
Une application et un site web
Notre projet vise à évaluer et à réduire l'émission de CO2 produite par les salariés d'une entreprise , lors du transport de leur domicile à leur lieu de travail.

Un challenge inter-entreprise
Pour ajouter un aspect compétitif, Ecovoito se définit comme un challenge entre entreprises. Ce défi se réalise tous les Trimestres

Une solution de covoiturage
Pour faciliter la mise en relation des salariés.

## API

Google Maps Directions API : itinéraire - calcul de la distance 
Places API : input autocomplete maps